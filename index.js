'use strict';

const {
	Application,
	BadRequest,
	NotAuthorized,
	NotAuthenticated,
} = require('Application');
const { Pool } = require('pg');
const { User } = require('User');
const config = require('./config.js');
const debug = require('debug')('http');

const app = module.exports = new Application();

const pages = {
	get: async (ctx) => {
		if(!(await ctx.user.getPermission(ctx.pool)).pages.list)
			throw new NotAuthorized();
		const res = await ctx.pool.query('SELECT name FROM pages.default;');
		ctx.res.body = res.rows;
	},
};

const page = {
	add: async (ctx) => {
		if(!(await ctx.user.getPermission(ctx.pool)).pages.add)
			throw new NotAuthorized();
		const name = decodeURI(ctx.req.url.substr(1));
		const body = ctx.req.body.toString().trim();
		try {
			const res = await ctx.pool.query('INSERT INTO pages.default (name, body) VALUES ($1::text, $2::text)', [name, body]);
			if(res.rowCount != 1)
				throw new Error('add page failed (no sql error)');
			ctx.res.code = 201;
		} catch(ex) {
			debug(ex);
			throw new BadRequest();
		}
	},
	del: async (ctx) => {
		if(!(await ctx.user.getPermission(ctx.pool)).pages.remove)
			throw new NotAuthorized();
		const name = decodeURI(ctx.req.url.substr(1));
		try {
			const res = await ctx.pool.query('DELETE FROM pages.default WHERE name = $1::text', [name]);
			if(res.rowCount != 1)
				throw new Error('delete page failed (no sql error)');
			ctx.res.code = 204;
		} catch(ex) {
			debug(ex);
			throw new BadRequest();
		}
	},
	post: async (ctx) => {
		if(!(await ctx.user.getPermission(ctx.pool)).pages.edit)
			throw new NotAuthorized();
		const name = decodeURI(ctx.req.url.substr(1));
		const body = ctx.req.body.toString().trim();
		try {
			const res = await ctx.pool.query('UPDATE pages.default SET body = $2::text WHERE name = $1::text', [name, body]);
			if(res.rowCount != 1)
				throw new Error('modify page failed (no sql error)');
			ctx.res.code = 204;
		} catch(ex) {
			debug(ex);
			throw new BadRequest();
		}
	},
	get: async (ctx) => {
		if(!(await ctx.user.getPermission(ctx.pool)).pages.view)
			throw new NotAuthorized();
		const res = await ctx.pool.query('SELECT body FROM pages.default WHERE name = $1::text', [decodeURI(ctx.req.url.slice(1))]);
		if(res.rowCount == 1) {
			ctx.res.headers['Content-Type'] = 'text/markdown';
			ctx.res.body = res.rows[0].body;
		}
	}
};

app.use(async (ctx) => {
	ctx.pool = new Pool(config.database);
});
app.use(async (ctx) => {
	ctx.user = new User();

	if(
		typeof ctx.req.headers['authorization'] !== 'undefined' &&
		!await ctx.user.check(ctx)
	)
		throw new NotAuthenticated();
});
app.use(async (ctx, next) => {
	await next();
	debug(`${ctx.req.method} ${ctx.req.url} ${ctx.res.code}`);
});
app.use(async (ctx, next) => {
	await next();

	switch(ctx.req.method) {
		case 'GET':
			if(ctx.req.url == '/')
				await pages.get(ctx);
			else
				await page.get(ctx);
			break;
		case 'POST':
			if(ctx.req.url != '/')
				await page.post(ctx);
			break;
		case 'PUT':
			if(ctx.req.url != '/')
				await page.add(ctx);
			break;
		case 'DELETE':
			if(ctx.req.url != '/')
				await page.del(ctx);
			break;
	}
});

app.listen(config.server.port, () => {
	debug(`listening on http://0.0.0.0:${config.server.port}/`);
});
