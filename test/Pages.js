const assert = require('assert');
const Pages = require('../index.js');
const http = require('http');
const { Pool } = require('pg');
const config = require('../config.js');

describe('Pages', function() {
	const port = 9091
	const pool = new Pool(config.database);
	
	const test = {
		name: '/test',
		content: 'This is a test page',
		modified: 'This is a modified page',
	};

	const user = {
		name: 'aladdin',
		pass: 'open sesame',
		key: '',
		get auth() {
			return `${this.name}:${this.key}`;
		},
		get header() {
			return `Basic ${Buffer.from(this.auth).toString('base64')}`;
		}
	};

	function request(page, options, done, cb) {
		return http.request(`http://localhost:${port}${page}`, options, (res) => {
			if(typeof cb !== 'undefined')
				cb(res);
		}).on('error', (e) => {
			done(e);
		});
	}
	function parse(res, done, cb) {
		res.body = [];
		res.on('data', (chunk) => {
			res.body.push(chunk)
		}).on('end', () => {
			res.body = Buffer.concat(res.body).toString()
			cb(res);
		}).on('error', (e) => {
			done(e);
		});
	}
	function get(page, u, done, cb) {
		const options = {
			method: 'GET',
		};
		if(u && u.key !== '')
			options.headers = {
				'authorization': u.header,
			};
		return request(page, options, done, cb).end();
	}
	function del(page, u, done, cb) {
		const options = {
			method: 'DELETE',
		};
		if(u && u.key !== '')
			options.headers = {
				'authorization': u.header,
			};
		return request(page, options, done, cb).end();
	}
	function put(page, u, data, done, cb) {
		const options = {
			method: 'PUT',
		};
		if(u && u.key !== '')
			options.headers = {
				'authorization': u.header,
			};
		return request(page, options, done, cb).end(data);
	}
	function post(page, u, data, done, cb) {
		const options = {
			method: 'POST',
		};
		if(u && u.key !== '')
			options.headers = {
				'authorization': u.header,
			};
		return request(page, options, done, cb).end(data);
	}

	before(function(done) {
		Pages.close(() => {
			Pages.listen(port, () => {
				pool.query(
					'SELECT users.create($1::text, $2::text)',
					[user.name, user.pass]
				).then(() => {
					pool.query(
						'INSERT INTO users.users_x_permissions ' +
						'SELECT $1::text, id FROM users.permissions, ' +
						'LATERAL jsonb_each(data) WHERE app = $2::text ' +
						'AND jsonb_each.value::bool',
						[user.name, 'pages']
					).then(() => {
						pool.query(
							'INSERT INTO users.sessions SELECT name FROM users.users ' +
							'WHERE name = $1::text AND hash = crypt($2::text, hash) ' +
							'RETURNING key',
							[user.name, user.pass]
						).then((res) => {
							user.key = res.rows[0].key;
							done();
						}).catch((e) => done(e));
					}).catch((e) => done(e));
				}).catch((e) => done(e));
			});
		});
	});
	after(function(done) {
		Pages.close(() => {
			pool.query('DELETE FROM users.sessions_real WHERE name = $1::text', [user.name]).then(() => {
				pool.query('DELETE FROM users.users_x_permissions WHERE "user" = $1::text', [user.name]).then(() => {
					pool.query('DELETE FROM users.users WHERE name = $1::text', [user.name]).then(() => {
						done();
					}).catch((e) => done(e));
				}).catch((e) => done(e));
			}).catch((e) => done(e));
		});
	});

	describe('#get', function() {
		it('should return with json on GET /', function(done) {
			get('/', null, done, (res) => {
				assert.equal(res.headers['content-type'], 'application/json');
				done();
			});
		});
		it('should return markdown on GET /Home', function(done) {
			get('/Home', null, done, (res) => {
				assert.equal(res.headers['content-type'], 'text/markdown');
				parse(res, done, (res) => {
					assert(typeof res.body !== 'undefined');
					assert(typeof res.body !== 'array');
					assert(res.body.length !== 0);
					done();
				});
			});
		});
		it('should reject adding when no permission', function(done) {
			put(test.name, null, test.content, done, (res) => {
				assert.equal(res.statusCode, 403);
				done();
			});
		});
		it('should be able to add pages', function(done) {
			put(test.name, user, test.content, done, (res) => {
				assert.equal(res.statusCode, 201);
				done();
			});
		});
		it('should be able to retreive the new page', function(done) {
			get(test.name, null, done, (res) => {
				parse(res, done, (res) => {
					assert.equal(res.body, test.content);
					done();
				});
			});
		});
		it('should reject modifying when no permission', function(done) {
			post(test.name, null, test.modified, done, (res) => {
				assert.equal(res.statusCode, 403);
				done();
			});
		});
		it('should be able to modify pages', function(done) {
			post(test.name, user, test.modified, done, (res) => {
				assert.equal(res.statusCode, 204);
				done();
			});
		});
		it('should then return the modified page', function(done) {
			get(test.name, null, done, (res) => {
				parse(res, done, (res) => {
					assert.equal(res.body, test.modified);
					done();
				});
			});
		});
		it('should reject modifying when no permission', function(done) {
			del(test.name, null, done, (res) => {
				assert.equal(res.statusCode, 403);
				done();
			});
		});
		it('should be able to delete pages', function(done) {
			del(test.name, user, done, (res) => {
				assert.equal(res.statusCode, 204);
				done();
			});
		});
		it('should then be gone', function(done) {
			get(test.name, null, done, (res) => {
				assert.equal(res.statusCode, 404);
				done();
			});
		});
	});
});
